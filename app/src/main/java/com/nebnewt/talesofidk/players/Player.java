package com.nebnewt.talesofidk.players;

import com.nebnewt.talesofidk.Position;
import com.nebnewt.talesofidk.networking.PlayerNetworkData;

public class Player extends Character implements CharacterInterface{

    private int remainingPoints; //thingies you can use for increasing your fancy stats


    public Player()
    {
        this.level=0;
    }
    public void levelUp()
    {
        level++;
    }

    public void setName(String name)
    {
        this.name= name;
    }

    public boolean increaseStrength()
    {
        if(remainingPoints>0)
        {
            strength++;
            remainingPoints--;
            return true;
        }
        return false;
    }

    public boolean increaseMagic()
    {
        if(remainingPoints>0)
        {
            magic++;
            remainingPoints--;
            return true;
        }
        return false;
    }

    //TODO reszta

}
