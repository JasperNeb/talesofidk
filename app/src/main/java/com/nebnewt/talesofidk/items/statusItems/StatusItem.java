package com.nebnewt.talesofidk.items.statusItems;

import com.nebnewt.talesofidk.items.Item;

public abstract class StatusItem extends Item implements StatusItemInterface{

    @Override
    public String getType()
    {
        return "STATUS_ITEM";
    }
}
