package com.nebnewt.talesofidk.items.clothing;

import com.nebnewt.talesofidk.items.ItemInterface;

public class BasicClothing extends Armor implements ItemInterface {

    public BasicClothing()
    {
        weight=1;
        startingLevel=1;
        defense=2;
        magicDefense=2;
        name="Basic Clothing";
        description="Some plain clothes. Not really fancy.";
    }
}
