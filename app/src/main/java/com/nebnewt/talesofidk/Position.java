package com.nebnewt.talesofidk;

import org.json.JSONException;
import org.json.JSONObject;

public class Position implements GameNetworkingInterface{

    private int baseSpeed=2;
    private int nearbyLimit=5;
    private int x;
    private int y;
    public Position(int x, int y)
    {
        this.x=x;
        this.y=y;
    }

    public boolean checkIfNearby(Position p)
    {
        return (Math.abs(p.x-this.x)<nearbyLimit || Math.abs(p.y=this.y)<nearbyLimit);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void moveUp()
    {
        this.y+=baseSpeed;
    }

    public void moveDown()
    {
        this.y-=baseSpeed;
    }
    public void moveLeft()
    {
        this.x-=baseSpeed;
    }
    public void moveRight()
    {
        this.x+=baseSpeed;
    }

    @Override
    public JSONObject convertToJSONObject() {
        JSONObject positionJsonObject=new JSONObject();

        try {
            positionJsonObject.put("id", HardcodedShit.id);
            positionJsonObject.put("x", x);
            positionJsonObject.put("y", y);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return positionJsonObject;
    }
}
