package com.nebnewt.talesofidk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nebnewt.talesofidk.networking.FetchOtherPlayersAsyncTask;
import com.nebnewt.talesofidk.networking.PlayerNetworkData;
import com.nebnewt.talesofidk.networking.PostMyPositionAsyncTask;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.SECONDS;

public class StartingActivity extends AppCompatActivity implements FetchOtherPlayersAsyncTask.FetchOtherPlayersCallback {

    final String myUrl = "http://moje.halamix2.pl/api/v0/";
    Gson gson;
    TextView textView;
    List<PlayerNetworkData> playerNetworkData;
    Position position;

    Type userListType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting);

        position = new Position(69, 69);
        playerNetworkData=new ArrayList<>();
        gson=new Gson();
        textView=(TextView)findViewById(R.id.showposition_textview);
        userListType = new TypeToken<ArrayList<PlayerNetworkData>>(){}.getType();
        beepForAnHour();

       /* ArrayList<Item> items= new ArrayList<>();
        items.add(new BasicClothing());
        items.add(new BigLeaf());
        items.add(new BasicSword());

        for(Item i : items)
        {
            Log.i("LOGI LOGI", i.getName());
        }*/


    }

    @Override
    public void onOtherPlayersDataReceived(String result) {

        playerNetworkData=gson.fromJson(result, userListType);

        textView.setText(textBuilder());
    }

    public void goUp(View v)
    {
        position.moveUp();

        firePostPositionAsyncTask();
    }

    public void goDown(View v)
    {
        position.moveDown();

        firePostPositionAsyncTask();
    }

    public void goLeft(View v)
    {
        position.moveLeft();

        firePostPositionAsyncTask();
    }

    public void goRight(View v)
    {
        position.moveRight();

        firePostPositionAsyncTask();
    }

    private void firePostPositionAsyncTask()
    {
        new PostMyPositionAsyncTask().execute(myUrl+"set", position.convertToJSONObject().toString());
    }

    private void fireFetchOtherPlayersAsyncTask()
    {
        new FetchOtherPlayersAsyncTask(this).execute(myUrl+"get", null);
    }

    private String textBuilder()
    {
        StringBuilder sb = new StringBuilder();

        for(PlayerNetworkData p : playerNetworkData)
        {
            sb.append(p.getName()+" X: "+p.getX()+" Y: "+p.getY()+"\n");
        }

        return sb.toString();
    }

    private final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

    public void beepForAnHour() {
        final Runnable beeper = new Runnable() {
            public void run() { System.out.println("beep");
            fireFetchOtherPlayersAsyncTask();
             }
        };

        final ScheduledFuture<?> beeperHandle =
                scheduler.scheduleAtFixedRate(beeper, 1, 1, SECONDS);
        scheduler.schedule(new Runnable() {
            public void run() { beeperHandle.cancel(true); }
        }, 60 * 60, SECONDS);
    }


}
