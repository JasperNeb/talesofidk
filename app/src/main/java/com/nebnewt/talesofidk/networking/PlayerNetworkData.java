package com.nebnewt.talesofidk.networking;

import com.nebnewt.talesofidk.Position;
import com.nebnewt.talesofidk.players.Player;

public class PlayerNetworkData {

    private Position position;
    private String name;
    private int id;

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getX()
    {
        return position.getX();
    }

    public int getY()
    {
        return position.getY();
    }
}
