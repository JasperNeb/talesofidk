package com.nebnewt.talesofidk.items;

public interface ItemInterface {

    int getTakenSpace();
    float getWeight();
    String getName();
    String getDescription();
    String getType(); //TODO to raczej powinno byc ogarniete inaczej ...
}
