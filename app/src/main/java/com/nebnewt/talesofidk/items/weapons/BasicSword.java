package com.nebnewt.talesofidk.items.weapons;

import com.nebnewt.talesofidk.items.ItemInterface;

public class BasicSword extends Weapon implements ItemInterface {

    public BasicSword()
    {
        atk=5;
        magic=1;
        name="Basic Sword";
        description="Basic sword for beginners";
        takenSpace=2;
        weight=3;
    }
}
