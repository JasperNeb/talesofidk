package com.nebnewt.talesofidk.items.clothing;

import com.nebnewt.talesofidk.items.Equipment;
import com.nebnewt.talesofidk.items.ItemInterface;

public abstract class Armor extends Equipment implements ItemInterface {

    protected int defense;
    protected int magicDefense;

    public Armor()
    {
        weight=1;
        takenSpace=2;
        defense=1;
        magicDefense=1;
        startingLevel=1;
    }

    @Override
    public int getTakenSpace() {
        return takenSpace;
    }

    @Override
    public float getWeight() {
        return weight;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getType()
    {
        return "Armor";
    }

}
