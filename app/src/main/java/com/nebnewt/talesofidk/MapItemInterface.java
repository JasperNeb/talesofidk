package com.nebnewt.talesofidk;

public interface MapItemInterface {
    Position getPosition();
    boolean setPosition(Position p);
    boolean setPosition(int x, int y);
}
