package com.nebnewt.talesofidk.players;

import com.nebnewt.talesofidk.MapItemInterface;
import com.nebnewt.talesofidk.Position;
import com.nebnewt.talesofidk.items.clothing.Armor;
import com.nebnewt.talesofidk.items.weapons.Weapon;

public abstract class Character implements CharacterInterface, MapItemInterface{

    protected String name="Unknown";
    protected int level;
    protected Position position;
    protected Weapon EquipedWeapon;
    protected Armor EquipedArmor;

    protected int strength, magic, luck, defense, magicDefense, speed;

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public void moveUp() {

    }

    @Override
    public void moveLeft() {

    }

    @Override
    public void moveDown() {

    }

    @Override
    public void moveRight() {

    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public boolean setPosition(Position p) {
        this.position=p;
        return true;
    }

    @Override
    public boolean setPosition(int x, int y) {
        this.position= new Position(x,y);
        return true;
    }
}
