package com.nebnewt.talesofidk.items;

import com.nebnewt.talesofidk.items.bonuses.BonusInterface;
import com.nebnewt.talesofidk.items.improvementItems.ImprovementItemInterface;

public abstract class Equipment extends Item implements ItemInterface{

    protected int startingLevel;
    protected BonusInterface[] bonusSlots;
    protected ImprovementItemInterface[] improvementSlots;

    public BonusInterface getBonus(int slot) { //TODO rename this function maybe?
        if(slot<bonusSlots.length)
        {
            return bonusSlots[slot];

        }

        throw new IndexOutOfBoundsException("slot value is bigger than tab size");
    }

    public boolean setBonusSlot(int slot, BonusInterface bonus) {
        if(slot<bonusSlots.length)
        {
            bonusSlots[slot]=bonus;
            return true;
        }

        return false;
    }

    public ImprovementItemInterface getImprovementSlot(int slot) {
        if(slot<improvementSlots.length)
        {
            return improvementSlots[slot];
        }

        throw new IndexOutOfBoundsException("slot value is bigger than tab size");
    }

    public boolean setImprovementSlots(int slot, ImprovementItemInterface improvementItem) {
        if (slot < improvementSlots.length)
        {
            improvementSlots[slot]=improvementItem;
            return true;
        }

        return false;
    }

    public int getBonusSlotsNumber()
    {
        return bonusSlots.length;
    }

    public int getImprovementItemsSlotsNumber()
    {
        return improvementSlots.length;
    }
}
