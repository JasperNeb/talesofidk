package com.nebnewt.talesofidk.items.statusItems;

import android.util.Log;

import com.nebnewt.talesofidk.items.Item;

public class Tea extends StatusItem implements StatusItemInterface {

    public Tea()
    {
        name="Tea";
        description="Tasty tea. Slightly restores HP";
        weight=0.1f;
        takenSpace=1;
    }
    @Override
    public int getTakenSpace() {
        return takenSpace;
    }

    @Override
    public float getWeight() {
        return weight;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void use() {

        Log.i("TEA CLASS","mniam mniam, pyszne");
    }
}
