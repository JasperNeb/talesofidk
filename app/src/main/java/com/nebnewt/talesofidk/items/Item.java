package com.nebnewt.talesofidk.items;

public abstract class Item implements ItemInterface {
    protected float weight;
    protected int dropChange = 100; //0-100%
    protected int takenSpace;
    protected String name, description;
}
