package com.nebnewt.talesofidk.items.weapons;


import com.nebnewt.talesofidk.items.Equipment;
import com.nebnewt.talesofidk.items.ItemInterface;

public abstract class Weapon extends Equipment implements ItemInterface {


    protected int atk;
    protected int magic;


    public Weapon()
    {
        atk=0;
        name="undefined";
        description="undefined";
        weight=3;
        takenSpace=2;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAtk() {
        return atk;
    }

    public void setAtk(int atk) {
        this.atk = atk;
    }

    @Override
    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public int getTakenSpace() {
        return takenSpace;
    }

    public void setTakenSpace(int takenSpace) {
        this.takenSpace = takenSpace;
    }
    public int getMagic() {
        return magic;
    }

    public void setMagic(int magic) {
        this.magic = magic;
    }

    @Override
    public String getType()
    {
        return "WEAPON";
    }
}
