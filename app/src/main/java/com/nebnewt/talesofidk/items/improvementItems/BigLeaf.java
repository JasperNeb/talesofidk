package com.nebnewt.talesofidk.items.improvementItems;

import com.nebnewt.talesofidk.items.Item;

public class BigLeaf extends ImprovementItem implements ImprovementItemInterface{

    public BigLeaf()
    {
        takenSpace=1;
        weight=0;
        name="Big leaf";
        description="A pretty big leaf. Slightly increases defense.";
    }

    @Override
    public int getTakenSpace() {
        return takenSpace;
    }

    @Override
    public float getWeight() {
        return weight;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
