package com.nebnewt.talesofidk.networking;

import android.os.AsyncTask;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class PostMyPositionAsyncTask extends AsyncTask<String, Void, String> {

  //  PostMyPositionCallback listener;



    @Override
    protected String doInBackground(String... params) {

        HttpURLConnection httpURLConnection = null;
        StringBuilder stringBuilder=new StringBuilder();
        try {

            httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
            httpURLConnection.setRequestMethod("POST");

            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestProperty("Content-Type", "application/json");

            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            wr.writeBytes(params[1]);
            Log.i("params0", params[0]);
            Log.i("params1", params[1]);
            wr.flush();
            wr.close();

            InputStream in = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(in);

            int inputStreamData = inputStreamReader.read();
            while (inputStreamData != -1) {
                char current = (char) inputStreamData;
                inputStreamData = inputStreamReader.read();
                stringBuilder.append(current);
                Log.i("MAMTO:", stringBuilder.toString());
            }
        } catch (Exception e) {
            Log.i("wyjebalo wyjontek", "i chuj");
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }

        Log.i("wynik", stringBuilder.toString());
        return stringBuilder.toString();
    }

    protected void onPostExecute(String result) {
       // listener.onResultReceived(result);

    }

    public interface PostMyPositionCallback {
        void onResultReceived(String result);
    }
}
