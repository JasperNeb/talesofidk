package com.nebnewt.talesofidk.items.improvementItems;

import com.nebnewt.talesofidk.items.Item;

public abstract class ImprovementItem extends Item implements ImprovementItemInterface {

    @Override
    public String getType()
    {
        return "IMPROVEMENT_ITEM";
    }
}
