package com.nebnewt.talesofidk.networking;

import android.os.AsyncTask;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class FetchOtherPlayersAsyncTask extends AsyncTask<String, Void, String>  {

    FetchOtherPlayersCallback listener;

    public FetchOtherPlayersAsyncTask(FetchOtherPlayersCallback listener)
    {
        this.listener=listener;
    }

    @Override
    protected String doInBackground(String... params) {

        HttpURLConnection httpURLConnection = null;
        StringBuilder stringBuilder=new StringBuilder();
        try {

            httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
            Log.i("params0", params[0]);

            httpURLConnection.setRequestMethod("GET");

            Log.i("params0", params[0]);

            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(false);
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestProperty("Accept", "application/json");


            int responseCode = httpURLConnection.getResponseCode();
            Log.i("RESPONSEKODE", Integer.toString(responseCode));
            InputStream in = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(in);

            Log.i("params0", params[0]);

            int inputStreamData = inputStreamReader.read();
            Log.i("params0", params[0]);

            while (inputStreamData != -1) {
                Log.i("params0", params[0]);

                char current = (char) inputStreamData;
                inputStreamData = inputStreamReader.read();
                stringBuilder.append(current);
                Log.i("MAMTO:", stringBuilder.toString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }

        }

        Log.i("wynik", stringBuilder.toString());
        return stringBuilder.toString();
    }

    protected void onPostExecute(String result) {
        listener.onOtherPlayersDataReceived(result);
    }

    public interface FetchOtherPlayersCallback {
        void onOtherPlayersDataReceived(String result);
    }
}
