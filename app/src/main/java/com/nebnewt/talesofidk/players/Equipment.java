package com.nebnewt.talesofidk.players;

import com.nebnewt.talesofidk.items.ItemInterface;

import java.util.ArrayList;

public class Equipment {

    private float currentItemsWeight=0;
    private int maxWeight=30;
    private ArrayList<ItemInterface> items;

    public Equipment()
    {
        items= new ArrayList<>();
    }

    public boolean addItem(ItemInterface i)
    {
        if(currentItemsWeight+i.getWeight()<maxWeight)
        {
            currentItemsWeight+=i.getWeight();
            items.add(i);
            return true;
        }

        return false;
    }

    public boolean removeItem()
    {
        return false;
    }

    private void updateCurrentItemsWeight()
    {
        float weight=0;
        for(ItemInterface item : items)
        {
            weight+=item.getWeight();
        }

        currentItemsWeight=weight;
    }
}
