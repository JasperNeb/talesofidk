package com.nebnewt.talesofidk.players;

import com.nebnewt.talesofidk.Position;

public interface CharacterInterface {

    void moveUp();
    void moveLeft();
    void moveDown();
    void moveRight();
    String getName();
    int getLevel();

}
