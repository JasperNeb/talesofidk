package com.nebnewt.talesofidk;

import org.json.JSONObject;

public interface GameNetworkingInterface {

    JSONObject convertToJSONObject();
}
